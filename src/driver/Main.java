package driver;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        URL url = getClass().getResource("/view/ScoreBoardManager.fxml");

        if (url == null) {
            System.out.println("No FXML File found");
            Platform.exit();
        }
        else{
            Parent root = FXMLLoader.load(url);
            primaryStage.setTitle("Score Board Manager");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        }

    }
}
