package driver;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.net.URL;
import java.util.ResourceBundle;

public class ScoreBoardManagerController implements Initializable {

    @FXML
    public Button btnOneRun, btnTwoRuns, btnThreeRuns, btnFourRuns, btnFiveRuns, btnSixRuns, btnSevenRuns, btnUndo;
    @FXML
    public Button btnIncrementOneBall, btnDecrementOneBall;
    @FXML
    public Button btnIncrementOneWicket, btnDecrementOneWicket;
    @FXML
    public ToggleButton toggleButtonToWinOrRB;
    @FXML
    public ImageView logoAPL;

    private ProjectorManager firstController = null;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/ProjectorSB.fxml"));

            Parent root1 = loader.load();

            firstController = loader.getController();

            Stage stage = new Stage();



            stage.setTitle("ATMIYA PREMIERE LEAGUE 2019");
            Image image = new Image("img/APL_Logo_2019.jpg");
            logoAPL.setImage(image);
            stage.setScene(new Scene(root1));


            stage.show();

        } catch (Exception e) {

            System.out.println("Error :" + e);

        }
    }

    public void incrementRuns(ActionEvent event) {
        if (event.getSource().equals(btnOneRun)) {
            firstController.getScoreManager().addRuns(1);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnTwoRuns)) {
            firstController.getScoreManager().addRuns(2);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnThreeRuns)) {
            firstController.getScoreManager().addRuns(3);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnFourRuns)) {
            firstController.getScoreManager().addRuns(4);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnFiveRuns)) {
            firstController.getScoreManager().addRuns(5);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnSixRuns)) {
            firstController.getScoreManager().addRuns(6);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnSevenRuns)) {
            firstController.getScoreManager().addRuns(7);
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnUndo)) {
            firstController.getScoreManager().decrementRun();
        }
        else if (event.getSource().equals(btnIncrementOneWicket)) {
            firstController.getScoreManager().addWicket();
        }
        else if (event.getSource().equals(btnDecrementOneWicket)) {
            firstController.getScoreManager().decrementWicket();
        }
        else if (event.getSource().equals(btnIncrementOneBall)) {
            firstController.getScoreManager().addBall();
        }
        else if (event.getSource().equals(btnDecrementOneBall)) {
            firstController.getScoreManager().decrementBall();
        }
        else if (event.getSource().equals(toggleButtonToWinOrRB)) {
            firstController.toggleInning();
        }
        firstController.updateUi();
    }
}
